import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfElements, counter;

        counter = 0;
        System.out.println("Введите количество элементов в массиве:");
        numberOfElements = scanner.nextInt();

        int[] array = new int[numberOfElements];

        System.out.println("Введите целые числа, которые будут содержаться в массиве:");

        for (int i = 0; i < numberOfElements; i++) {
            array[i] = scanner.nextInt(); // Заполняем массив элементами, введёнными с клавиатуры
        }


        for(int i = 0; i < numberOfElements; i++) {
            if (i == 0) {
                if (array[i] < array[i + 1]) {
                    counter ++;
                }
            }
            else if (i == numberOfElements - 1) {
                if (array[i] < array[i - 1]) {
                    counter++;
                }
            }
            else if (i > 0 && i < numberOfElements) {
                if (array[i] < array[i - 1] && array[i] < array[i + 1]) {
                    counter++;
                }
            }
        }

        System.out.println("Количество локальных минимумов в массиве: " + counter);

    }
}