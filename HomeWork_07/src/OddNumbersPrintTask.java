public class OddNumbersPrintTask extends AbstractNumbersPrintTask {

    public OddNumbersPrintTask(int from, int to) {
        super(from, to);
    }

    public void complete() {
        for (int i = from; i <= to; i++) {
            if (i % 2 == 1) {
                if ((i==to) | (i == to-1)){
                    System.out.println(i);
                } else{
                    System.out.print(i + " ");
                }
            }
        }
    }
}
