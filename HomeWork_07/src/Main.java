public class Main {
    public static void completeAllTasks (Task[]tasks) {
        for (int i =0 ; i < tasks.length; i++){
            tasks[i].complete();
        }
    }
    public static void main(String[] args) {
        AbstractNumbersPrintTask evenNumbersPrintTask = new EvenNumbersPrintTask(3, 10);
        AbstractNumbersPrintTask oddNumbersPrintTask = new OddNumbersPrintTask(-1, 18);

        Task[]tasks = {evenNumbersPrintTask, oddNumbersPrintTask};

        completeAllTasks(tasks);
    }
}