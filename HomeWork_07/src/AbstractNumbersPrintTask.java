public abstract class AbstractNumbersPrintTask implements Task{
    protected int from;
    protected int to;

    public AbstractNumbersPrintTask (int from, int to){
        this.from = from;
        this.to = to;
    }

    public abstract void complete();
}

