
import java.util.ArrayList;
import java.util.HashMap;


public class Main {
    public static void main(String[] args) {

        HashMap<Object, Integer> map = new HashMap<>();
        String myString = "Hello Hello bye Hello bye Inno";
        int maxValue = 0;
        String maxKey = "";
        String[] array = myString.split(" ");

        for (int i = 0; i < array.length; i++){
            if (map.containsKey(array[i])){
                int value = map.get(array[i]);
                map.put(array[i], value+1);
                if (maxValue < map.get(array[i])){
                    maxValue = map.get(array[i]);
                    maxKey = array[i];
                }
            } else {
                map.put(array[i], 1);
            }
        }

        System.out.println(maxKey + " " + maxValue);


    }
}