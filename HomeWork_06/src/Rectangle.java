public class Rectangle extends Figure{
    protected int height;
    protected int width;

    public Rectangle (int centerX, int centerY, int height, int width){
        super(centerX, centerY);
        this.height = height;
        this.width = width;
    }

    public double area (){
        return height*width;
    }

    public double perimeter (){
        return (height+width)*2;
    }

}
