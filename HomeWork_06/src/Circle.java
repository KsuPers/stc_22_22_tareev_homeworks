public class Circle extends Ellipse {

    public Circle(int centerX, int centerY, int smallRadius, int bigRadius){
        super(centerX, centerY, smallRadius, bigRadius);
        super.smallRadius = super.bigRadius;
    }

    public double area(){
        return Math.PI * smallRadius * smallRadius;
    }
    public double perimeter(){
        return 2*Math.PI*smallRadius;
    }

}
