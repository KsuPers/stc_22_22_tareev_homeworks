public class Square extends Rectangle {

    public Square (int centerX, int centerY, int height, int width){
        super(centerX, centerY, height, width);
        super.height = super.width;
    }

}
