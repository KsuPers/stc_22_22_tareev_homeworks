public class Ellipse extends Figure {
    protected int smallRadius;
    protected int bigRadius;

    public Ellipse(int centerX, int centerY, int smallRadius, int bigRadius){
        super(centerX, centerY);
        this.smallRadius = smallRadius;
        this.bigRadius = bigRadius;
    }

    public double area(){
        return Math.PI*(smallRadius/2)*(bigRadius/2);
    }

    public double perimeter(){
        return 2*Math.PI*Math.sqrt(((smallRadius*smallRadius)+(bigRadius*bigRadius))/8);
    }
}
