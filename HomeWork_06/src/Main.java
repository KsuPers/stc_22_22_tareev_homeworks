public class Main {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(0,0, 10,8);
        rectangle.move(1,-2);
        rectangle.area();
        rectangle.perimeter();

        Square square = new Square(0,0,5, 6);
        square.move(5,4);
        square.area();
        square.perimeter();

        Circle circle = new Circle(0,0,3,4);
        circle.move(2,4);
        circle.area();
        circle.perimeter();

        Ellipse ellipse = new Ellipse(0,0,4,8);
        ellipse.move(0,-5);
        ellipse.area();
        ellipse.perimeter();
    }
}