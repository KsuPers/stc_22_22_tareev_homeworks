import java.util.Arrays;

public abstract class Figure {
    private int centerX;
    private int centerY;

    public Figure(int centerX, int centerY){
        this.centerX = centerX;
        this.centerY = centerY;
    }

    public String move(int toX, int toY) {
        int coordinates[] = {centerX, centerY};

        centerX = toX;
        centerY = toY;

        return Arrays.toString(coordinates);
    }

    public abstract double area();

    public abstract double perimeter();
}