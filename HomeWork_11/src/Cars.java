public class Cars {
    private String number;
    private String model;
    private String color;
    private Integer mileage;
    private Integer cost;

    public Cars(String number, String model, String color, Integer mileage, Integer cost) {
        this.number = number;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.cost = cost;
    }

    public String getNumber() {
        return number;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public Integer getMileage() {
        return mileage;
    }

    public Integer getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return "Cars{" +
                "number='" + number + '\'' +
                ", model='" + model + '\'' +
                ", color='" + color + '\'' +
                ", mileage=" + mileage +
                ", cost=" + cost +
                '}';
    }
}
