import java.util.List;

public interface CarsRepository {
    List<String> numbersWithBlackColorOrZeroMileage(); //Номера всех автомобилей,имеющих черный цвет или нулевой пробег
    Integer uniqueModelsInCost700800(); //Количество уникальных моделей в ценовом диапазоне от700до800тыс
    String colorWithMinCost(); //Вывести цвет автомобиля с минимальной стоимостью
    Double middleCostOfCamry(); //Среднюю стоимость Camry

}
