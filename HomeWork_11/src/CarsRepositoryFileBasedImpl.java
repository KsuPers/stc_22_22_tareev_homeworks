import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CarsRepositoryFileBasedImpl implements CarsRepository {

    private final String fileName;

    public CarsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Cars> carMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");

        String number = parts[0];
        String model = parts[1];
        String color = parts[2];
        Integer mileage = Integer.parseInt(parts[3]);
        Integer cost = Integer.parseInt(parts[4]);

        return new Cars(number, model, color, mileage, cost);
    };


    @Override
    public List<String> numbersWithBlackColorOrZeroMileage() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            return bufferedReader
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getColor().equals("Black") || car.getMileage() == 0)
                    .map(Cars::getNumber)
                    .collect(Collectors.toList());

        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Integer uniqueModelsInCost700800() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            return bufferedReader
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getCost() >= 700000 && car.getCost() <= 800000)
                    .map(Cars::getModel)
                    .distinct().
                    toList()
                    .size();

        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public String colorWithMinCost() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            return bufferedReader
                    .lines()
                    .map(carMapper)
                    .min(Comparator.comparingInt(Cars::getCost))
                    .map(Cars::getColor)
                    .get();

        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Double middleCostOfCamry() {
        try (Reader fileReader = new FileReader(fileName);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){
            return bufferedReader
                    .lines()
                    .map(carMapper)
                    .filter(car -> car.getModel().equals("Camry"))
                    .mapToInt(Cars::getCost)
                    .average()
                    .getAsDouble();

        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }
}
