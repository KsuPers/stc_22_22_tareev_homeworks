public class Main {
    public static void main(String[] args) {

        CarsRepository carsRepository = new CarsRepositoryFileBasedImpl("cars.txt");

        System.out.println("Номера автомобилей, имеющих черный цвет или нулевой пробег: " + carsRepository.numbersWithBlackColorOrZeroMileage());
        System.out.println("Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.: " + carsRepository.uniqueModelsInCost700800());
        System.out.println("Цвет автомобиля с минимальной стоимостью: " + carsRepository.colorWithMinCost());
        System.out.println("Средняя стоимость Camry: " + carsRepository.middleCostOfCamry());
    }
}