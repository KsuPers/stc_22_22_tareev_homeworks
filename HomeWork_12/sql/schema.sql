create table _user (
                       id bigserial primary key,
                       first_name char(20) default 'DEFAULT_FIRSTNAME',
                       last_name char(20) default 'DEFAULT_LASTNAME',
                       phone_number char(12),
                       work_experience integer check (work_experience > 0),
                       age integer  check(age>= 0 and age<=120),
                       driving_license bool,
                       driving_license_category char(20),
                       rate integer check (rate >= 0 and rate <=5)
);

create table car (
                     model char(20),
                     color char(20),
                     number char (9),
                     id_car bigserial primary key,
                     id_owner integer not null,
                     foreign key (id_owner) references _user(id)
);

create table trip (
                      id_driver integer not null,
                      foreign key (id_driver) references _user(id),
                      id_car integer not null,
                      foreign key (id_car) references car(id_car),
                      date_trip timestamp,
                      length_trip integer
);