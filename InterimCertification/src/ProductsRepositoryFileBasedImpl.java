import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductsRepositoryFileBasedImpl implements ProductsRepository{

    private final String fileName;

    public ProductsRepositoryFileBasedImpl(String fileName) {
        this.fileName = fileName;
    }

    private static final Function<String, Product> productMapper = currentCar -> {
        String[] parts = currentCar.split("\\|");

        Integer id = Integer.parseInt(parts[0]);
        String name = parts[1];
        Double cost = Double.parseDouble(parts[2]);
        Integer amount = Integer.parseInt(parts[3]);

        return new Product(id, name, cost, amount);
    };

    private static final Function<Product, String> productStringFunction = product ->
            product.getId() + "|" + product.getName() + "|" + product.getCost() + "|" + product.getAmount();


    @Override
    public Product findById(Integer id) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))){
            return bufferedReader
                    .lines()
                    .map(productMapper)
                    .filter(product -> product.getId().equals(id))
                    .findAny()
                    .get();

        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Product> findAllByTitleLike(String title) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))){
            return bufferedReader
                    .lines()
                    .map(productMapper)
                    .filter(product -> product.getName().contains(title))
                    .collect(Collectors.toList());

        } catch (IOException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Product product) {
        List<Product> products = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName))) {
            String currentProduct = null;

            while ((currentProduct = bufferedReader.readLine()) != null) {
                Product searching = productMapper.apply(currentProduct);
                if (searching.getId().equals(product.getId())) {
                    products.add(product);
                } else {
                    products.add(productMapper.apply(currentProduct));
                }
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false))) {
            for (int i = 0; i < products.size(); i++) {
                String currentLine = productStringFunction.apply(products.get(i));
                bufferedWriter.write(currentLine);
                if (i != products.size()-1){
                    bufferedWriter.newLine(); //Если вы
                }
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
