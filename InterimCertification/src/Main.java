public class Main {
    public static void main(String[] args) {
        ProductsRepositoryFileBasedImpl productsRepository = new ProductsRepositoryFileBasedImpl("Products.txt");
        System.out.println("Продукт по заданному id: " + productsRepository.findById(15));
        System.out.println("Список продуктов, содержащих искомую комбинацию букв в названии: " + productsRepository.findAllByTitleLike("оло"));

        Product coconutMilk = productsRepository.findById(89);

        coconutMilk.setAmount(11);
        coconutMilk.setCost(40.1);

        productsRepository.update(coconutMilk);
    }
}