public class Product {
    private Integer id;
    private String name;
    private Double cost;
    private Integer amount;

    public Product(Integer id, String name, Double cost, Integer amount) {
        this.id = id;
        this.name = name;
        this.cost = cost;
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getCost() {
        return cost;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", cost=" + cost +
                ", amount=" + amount +
                '}';
    }
}
