public class ATM {

    public static final int OPERATION_START = 0;
    private int sumOfRemainigMoneyInATM; //Сумма оставшихся денег в банкомате
    private int maxSumToGet; //Максимальная сумма, разрешенная к выдаче
    private int maxAmountInATM; //Максимальный объем денег в банкомате
    private int amountOfOperation; //Количество проведенных операций

    public ATM(int sumOfRemainigMoneyInATM, int maxSumToGet, int maxAmountInATM) {
        this.sumOfRemainigMoneyInATM = sumOfRemainigMoneyInATM;
        this.maxSumToGet = maxSumToGet;
        this.maxAmountInATM = maxAmountInATM;
        this.amountOfOperation = OPERATION_START;
    }

    public int giveMoney(int money) {
        this.amountOfOperation++;

        if ((money <= sumOfRemainigMoneyInATM) | (money <= maxSumToGet)) {
            sumOfRemainigMoneyInATM -= money;
            return money;
        } else if (maxSumToGet > sumOfRemainigMoneyInATM){
            money = sumOfRemainigMoneyInATM;
            sumOfRemainigMoneyInATM = 0;
            return money;
        }
        else {
            sumOfRemainigMoneyInATM -= maxSumToGet;
            return maxSumToGet;
        }
    }

    public int putMoneyOnAccount(int money) {
        this.amountOfOperation++;

        if ((sumOfRemainigMoneyInATM + money) > maxAmountInATM) {
            return (sumOfRemainigMoneyInATM + money - maxAmountInATM);
        } else {
            sumOfRemainigMoneyInATM += money;
            return 0;
        }
    }
}
