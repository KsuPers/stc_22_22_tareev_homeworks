import java.util.Arrays;
public class ArraysTasksResolver {


    static void resolveTask(int[]array, ArrayTask task, int from, int to){
        System.out.println("Исходный массив: " + Arrays.toString(array));
        System.out.println("Значение from: " + from + ", значение to: " + to);
        System.out.println(task.resolve(array, from, to));

    }
}
