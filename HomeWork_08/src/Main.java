public class Main {
    public static void main(String[] args) {

       ArrayTask sumOfElements = (array, from, to) -> {
           int sumFinal = 0;
           for (int i = from; i <= to; i++){
               sumFinal += array[i];
           }
           return sumFinal;
       };

       ArrayTask sumOfNumbersMaxElement = (array, from, to) -> {
           int maxValue = 0;
           int maxValueSum = 0;
           //int partOfMaxValue = 0;
           for (int i = from; i <= to; i++) {
               if (maxValue < array[i]) {
                   maxValue = array[i];
               }
           }
           while (maxValue > 0){
               maxValueSum += maxValue%10;
               maxValue = maxValue/10;
           }
           return maxValueSum;
       };

       //ArraysTasksResolver task = new ArraysTasksResolver();
       int [] array = {12, 62, 4, 2, 100, 40, 56};

       ArraysTasksResolver.resolveTask(array, sumOfElements, 1,4 );
       ArraysTasksResolver.resolveTask(array,sumOfNumbersMaxElement,1,3);
    }
}

