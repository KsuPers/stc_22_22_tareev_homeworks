import java.util.Arrays;

public class Main {



    public static void main(String[] args) {
        List<String> stringList = new ArrayList<>();

        stringList.add("Hello!");
        stringList.add("Bye!");
        stringList.add("Fine!");
        stringList.add("C++!");
        stringList.add("Fine!");

        stringList.remove("Fine!");
       for (int i=0; i < stringList.size(); i ++){
           System.out.println(stringList.get(i));
       }

        System.out.println("");

        List<String> stringList1 = new ArrayList<>();

        stringList1.add("Hello!");
        stringList1.add("Bye!");
        stringList1.add("Fine!");
        stringList1.add("C++!");
        stringList1.add("Fine!");

        stringList1.removeAt(3);
        for (int i=0; i < stringList1.size(); i ++){
            System.out.println(stringList1.get(i));
        }
    }
}