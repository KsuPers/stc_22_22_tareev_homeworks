public class ArrayList<T> implements List<T> {

        private final static int DEFAULT_ARRAY_SIZE = 10;

        // поле-массив, которое хранит все элементы
        private T[] elements;

        // поле, котороые хранит число элементов
        private int count;

        public ArrayList() {
            this.elements = (T[]) new Object[DEFAULT_ARRAY_SIZE];
            this.count = 0;
        }

        @Override
        public void add(T element) {
            // если массив заполнен
            if (isFull()) {
                // изменяем его размер
                resize();
            }
            elements[count] = element;
            count++;
        }

        private void resize() {
            // берем старый размер массива
            int currentLength = elements.length;
            // получаем значение, в полтора раза больше предыдыщуего размерf
            // это и будет новый массив
            int newLength = currentLength + currentLength / 2;
            // создали новый массив бОльшего размера
            T[] newElements = (T[]) new Object[newLength];
            // последовательно копируем элементы
            for (int i = 0; i < count; i++) {
                newElements[i] = elements[i];
            }
            // переключить ссылку на новый массив
            this.elements = newElements;
        }

        private boolean isFull() {
            return count == elements.length;
        }

        @Override
        public void remove(T element) {

            if (contains(element)) {
                int currentLength = size();
                int newLength = currentLength - 1;
                T[] newElements = (T[]) new Object[newLength];
                int index = -1;
                for (int i = 0; i < currentLength; i++) {
                    if (elements[i].equals(element) & index == -1) {
                        index = i;
                    }
                }
                removeAt(index);
            }

        }

        @Override
        public boolean contains(T element) {
            // пробегаем все элементы
            for (int i = 0; i < count; i++) {
                // если элемент найден
                if (elements[i].equals(element)) {
                    return true;
                }
            }
            // если элемент так и не был найден - возвращаем false
            return false;
        }

        @Override
        public int size() {
            return count;
        }

        @Override
        public void removeAt(int index) {
            if (contains(elements[index])){
                int currentLength = size();
                int newLength = currentLength - 1;
                T[] newElements = (T[]) new Object[newLength];

                //elements[index] = (T) "null";
                for (int i = 0; i < index; i++){
                    newElements[i] = elements[i];
                }
                for (int i = index; i < newLength; i++){
                    newElements[i] = elements[i+1];
                }
                count -= 1;
                this.elements = newElements;
            }
        }

        @Override
        public T get(int index) {
            if (index >= 0 && index < count) {
                return elements[index];
            }
            return null;
        }
}
